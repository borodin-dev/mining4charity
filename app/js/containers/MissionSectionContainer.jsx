import React, { Component } from 'react';

import MiningSection from "../components/MiningSection";
import MissionSectionImage from '../components/MissionSectionImage';
import MissionSectionText from "../components/MissionSectionText";
import MissionSection from "../components/MissionSection";

class MissionSectionContainer extends Component {
  render() {
    return (
      <div className="mission-section" id="ourMission">
        <MissionSection />
        <div className="mission-section__content">
          <div className="row">
            <MissionSectionImage />
            <MissionSectionText />
          </div>
        </div>
      </div>
    );
  }
}

export default MissionSectionContainer;
