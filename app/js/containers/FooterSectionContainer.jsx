import React, { Component } from 'react';

import FooterSection from "../components/FooterSection";
import FooterSectionLow from "../components/FooterSectionLow";

class FooterSectionContainer extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="container footer-section">
          <FooterSection/>
          <FooterSectionLow/>
        </div>
      </div>
    );
  }
}

export default FooterSectionContainer;
