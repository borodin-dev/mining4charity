import React, { Component } from 'react';

import HelpUsSection from '../components/HelpUsSection'
import HelpUsSectionCoins from '../components/HelpUsSectionCoins'

class HelpUsSectionContainer extends Component {
    render() {
    return (
      <div className="help-us-section" id="donate">
        <HelpUsSection/>
        <HelpUsSectionCoins/>
      </div>
    );
  }
}

export default HelpUsSectionContainer;
