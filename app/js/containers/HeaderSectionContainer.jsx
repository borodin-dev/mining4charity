import React, { Component } from 'react';

import HeaderSection from '../components/HeaderSection'

class HeaderSectionContainer extends Component{
  render(){
    return(
      <HeaderSection/>
    );
  }
}

export default HeaderSectionContainer;
