import React, { Component } from 'react';

import HowItWorksSection from '../components/HowItWorksSection';
import HowItWorksContent from '../components/HowItWorksContent';

class HowItWorksContainer extends Component {
  render() {
    return (
      <div className="how-works-section" id="howWorks">
        <HowItWorksSection />
        <HowItWorksContent />
      </div>
    );
  }
}

export default HowItWorksContainer;
