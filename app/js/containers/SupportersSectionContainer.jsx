import React, { Component } from 'react';

import SupportersSection from '../components/SupportersSection';
import SupportersSectionBrands from '../components/SupportersSectionBrands';

class SupportersSectionContainer extends Component {
  render() {
    return (
      <div className="supporters-section" id="supporters">
        <SupportersSection/>
        <div>
          <SupportersSectionBrands />
        </div>
      </div>
    );
  }
}

export default SupportersSectionContainer;
