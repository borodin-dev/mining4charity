import React, { Component } from 'react';

import MiningSection from '../components/MiningSection';
import MiningSectionImage from '../components/MiningSectionImage';

class MiningSectionContainer extends Component{
  render(){
    return(
      <div className="row">
        <MiningSection/>
        <MiningSectionImage/>
      </div>
    );
  }
}

export default MiningSectionContainer;
