import React, { Component } from 'react';

import OrganizationsSection from '../components/OrganizationsSection';
import OrganizationsSectionBrands from '../components/OrganizationsSectionBrands';

class OrganizationsSectionContainer extends Component {
  render() {
    return (
      <div className="organizations-section">
        <OrganizationsSection />
        <OrganizationsSectionBrands />
      </div>
    );
  }
}

export default OrganizationsSectionContainer;
