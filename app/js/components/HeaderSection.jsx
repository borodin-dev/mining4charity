import React, { Component } from 'react';
import Scrollchor from 'react-scrollchor';

class HeaderSection extends Component{
  render(){
    return(
      <nav className="navbar navbar-expand-lg navbar-light d-flex">
        <a className="navbar-brand" href="#">
          <img
            src="./images/png/Logo.png"
            width="200"
            height="130"
            className="d-inline-block align-top img-fluid"
            alt="Logo" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
          <ul className="navbar-nav">
            <li className="nav-item">
              {/*<a className="nav-link" href="#">How it works</a>*/}
              <Scrollchor to="#howWorks" className="nav-link">How it works</Scrollchor>
            </li>
            <li className="nav-item">
              {/*<a className="nav-link" href="#">Our mission</a>*/}
              <Scrollchor to="#ourMission" className="nav-link">Our mission</Scrollchor>
            </li>
            <li className="nav-item">
              {/*<a className="nav-link" href="#">Donate</a>*/}
              <Scrollchor to="#donate" className="nav-link">Donate</Scrollchor>
            </li>
            <li className="nav-item">
              {/*<a className="nav-link" href="#">Our Supporters</a>*/}
              <Scrollchor to="#supporters" className="nav-link">Our Supporters</Scrollchor>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Contact</a>
              {/*<Scrollchor to="#contact" className="nav-link">Contact</Scrollchor>*/}
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default HeaderSection;
