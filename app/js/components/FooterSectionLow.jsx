import React, { Component } from 'react';


class FooterSectionLow extends Component {
  render() {
    return (
      <div className="row footer-section__low">
        <div className="col-lg-4 mail">
          <img
            src="./images/png/@.png"
            alt="mail"
          />
          <a href="mailto:contact@mining4charity.com">contact@mining4charity.com</a>
        </div>
        <div className="col-lg-4 copyright">
          <p>&#169; 2018 Mining4Charity</p>
        </div>
        <div className="col-lg-4 companies-sm-logo">
          <img
            src="./images/png/logo/CubexLogoSM.png"
            alt="CubeX Logo Small"
          />
          <img
            src="./images/png/logo/CubitsLogoSM.png"
            alt="Cubits Logo Small"
          />
        </div>
      </div>
    );
  }
}

export default FooterSectionLow;
