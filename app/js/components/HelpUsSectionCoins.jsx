import React, { Component } from 'react';
import CoinCard from './CoinCard';

class HelpUsSectionCoins extends Component {
  constructor() {
    super();
    this.state = {
      inputLinkClicked: false,
      copied: false,
      isFirstDisabled: true,
    };
    this.handleChange = this.handleChange.bind(this);
    this.toggleHidden = this.toggleHidden.bind(this);
  }
  toggleHidden() {
    this.setState({
      isHidden: !this.state.isHidden,
    });
  }
  handleChange(event) {
    this.setState({
      isHidden: !this.state.isHidden,
      value: event.target.value,
    });
  }
  render() {
    const Value = 'J1a4MTQs3jJclpLgL1HuCr7IDduShVxHM1X1nlUD';
    return (
      <div className="help-us-section__content">
        <div className="row coins-row">
          <CoinCard
            value = {Value}
            imageSrc="./images/png/coins/Bitcoin.png"
            imageAlt="Coin icon"
            coinTitle="Bitcoin"
          />
          <CoinCard
            value={Value}
            imageSrc="./images/png/coins/BitcoinCash.png"
            imageAlt="Coin icon"
            coinTitle="BitcoinCash"
          />
          <CoinCard
            value={Value}
            imageSrc="./images/png/coins/Litecoin.png"
            imageAlt="Coin icon"
            coinTitle="Litecoin"
          />
        </div>
        <div className="row coins-row-2">
          <CoinCard
            value={Value}
            imageSrc="./images/png/coins/Ethereum.png"
            imageAlt="Coin icon"
            coinTitle="Ethereum"
          />
          <CoinCard
            value={Value}
            imageSrc="./images/png/coins/Monero.png"
            imageAlt="Coin icon"
            coinTitle="Monero"
          />
          <CoinCard
            value={Value}
            imageSrc="./images/png/coins/Ripple.png"
            imageAlt="Coin icon"
            coinTitle="Ripple"
          />
        </div>
      </div>
    );
  }
}

export default HelpUsSectionCoins;
