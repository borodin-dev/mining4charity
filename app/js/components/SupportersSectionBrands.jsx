import React, { Component } from 'react';


class SupportersSectionBrands extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-lg-5 offset-lg-1 supporters-bike">
          <img
            className="logo-save img-fluid"
            src="./images/png/3men.png"
            alt="3 men"
          />
        </div>
        <div className="col-lg-3 offset-lg-1 supporters-btn-logo">
          <a href="http://qbex.io" target="_blank">
            <button className="cubex-logo">
              <img src="./images/png/logo/CubexLogo.png" alt="CubeX Logo" />
            </button>
          </a>
          <a href="https://cubits.com" target="_blank">
            <button className="cubits-logo">
              <img src="./images/png/logo/CubitsLogo.png" alt="Cubits Logo" />
            </button>
          </a>
        </div>
      </div>
    );
  }
}

export default SupportersSectionBrands;
