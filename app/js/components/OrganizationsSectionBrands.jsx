import React, { Component } from 'react';

class OrganizationsSectionBrands extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-lg-3 organizations-logo">
          <a href="https://www.savethechildren.org/" target="_blank">
            <img
              className="logo-save"
              src="./images/png/logo/SavethechildrenLogo.png"
              alt="Save the Children Logo"
              width="90"
            />
          </a>
        </div>
        <div className="col-lg-3 organizations-logo">
          <a href="http://www.yangrima.org/" target="_blank">
            <img
              className="logo-ybs"
              src="./images/png/logo/ybslogo.png"
              alt="YBS Logo"
              width="90"
            />
          </a>
        </div>
        <div className="col-lg-3 organizations-logo">
          <a href="https://www.greenpeace.org/" target="_blank">
            <img
              className="logo-green"
              src="./images/png/logo/Greenpeace-Logo.png"
              alt="Green Peace Logo"
              width="150"
            />
          </a>
        </div>
        <div className="col-lg-3 organizations-logo">
          <a href="http://www.abla.org/" target="_blank" >
            <img
              className="logo-abla"
              src="./images/png/logo/AblaPortugalLogo.png"
              alt="Alba Portugal Logo"
            />
          </a>
        </div>
        <img
          className=""
          src="./images/png/Line.png"
          alt="Line"
          width="186"
        />
      </div>
    );
  }
}

export default OrganizationsSectionBrands;
