import React, { Component } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';

class CoinCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    this.setState(function (prevState){
      return { isVisible: !prevState.isVisible }
    });
  }

  render() {
    return (
      <div className="col-lg-4">
        <div className="coin-block">
          <img
            src={this.props.imageSrc}
            alt={this.props.imageAlt}/>
          <p className="coin-block__title">{this.props.coinTitle}</p>
          <button
            className="coin-block__button"
            id="bitcoin"
            onClick={this.handleClick}
          >Wallet</button>
          {
            <div className={this.state.isVisible ? 'group-element' : 'group-element hidden'}>
              <input
                type="text"
                className="w-code"
                value={this.props.value}
                readOnly
                // disabled={true}
              />
              <CopyToClipboard
                text={this.props.value}
                onCopy={() => this.setState({ copied: true })}>
                <span className="copy-code"> <img src="./images/png/Copybt.png" alt="Copy"/></span>
              </CopyToClipboard>
            </div>
          }
        </div>
      </div>
    );
  }
}

export default CoinCard;
