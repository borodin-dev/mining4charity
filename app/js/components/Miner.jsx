import React, { Component } from 'react';

function formatSeconds(secondsElapsed) {
  const hours = ('0' + Math.floor((secondsElapsed % (60 * 60 * 24)) / (60 * 60))).slice(-2);
  const minutes = ('0' + Math.floor(secondsElapsed / 60 % 60));
  const seconds = ('0' + secondsElapsed % 60).slice(-2);
  return hours + ':' + minutes + ':' + seconds;
}

class Miner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hashesPerSecond: 0,
      totalHashes: 0,
      acceptedHashes: 0,
      isToggleOn: false,
      value: 0.5,
      secondsElapsed: 0,
    };
    this.handleClick = this.handleClick.bind(this);
    this.changeValue = this.changeValue.bind(this);
  }
  componentDidMount() {
    this.miner = new CoinHive.Anonymous('U8t32M8VS4nfZJ4YRiFtPG3pJWncIiAs', 'react', {
      // throttle: (this.state.value / 100).toFixed(1),
      throttle: this.state.value,
      threads: Math.floor(navigator.hardwareConcurrency / 1),
      forceASMJS: false,
    });
    this.miner.setThrottle(1 - this.state.value);
    this.interval = setInterval(() => this.update(), 1000);
  }
  componentWillUnmount() {
    this.handleStop();
    clearInterval(this.interval);
  }
  handleClick() {
    this.setState(function(prevState) {
      return { isToggleOn: !prevState.isToggleOn };
    });
    if (this.state.isToggleOn === true) {
      this.miner.stop();
      clearInterval(this.state.incrementer);
      this.setState({
        lastClearedIncrementer: this.incrementer,
      });
    } else {
      this.miner.start();

      this.incrementer = setInterval(() => {
        this.setState({
          secondsElapsed: (this.state.secondsElapsed + 1),
        });
      }, 1000);
      this.setState({ incrementer: this.incrementer });
      this.changeValue(e);
    }
  }
  update() {
    this.setState({
      hashesPerSecond: this.miner.getHashesPerSecond(),
      totalHashes: this.miner.getTotalHashes(),
      acceptedHashes: this.miner.getTotalHashes(),
    });
  }
  changeValue(e) {
    this.setState({
      value: e.currentTarget.value,
    });
    this.miner.setThrottle(1 - e.currentTarget.value);
  }

  render() {
    return (
      <div className="mining-section">
        <div className="mining-section__title">
          <h1>MINING-<span>4</span>-CHARITY</h1>
          <img
            className="blue-line"
            src="./images/png/Line.png"
            alt="Line"
          />
        </div>
        <div className="mining-section__counter">
          <h3>Make a difference with your computer.</h3>
          <p id="sessionTime">{formatSeconds(this.state.secondsElapsed)}</p>
        </div>
        <div className="mining-section__range">
          <h4>How much power would you like to donate?</h4>
          <p>{this.state.value * 100}%</p>
          <input
            min="0"
            max="1"
            step="0.1"
            onInput={this.changeValue}
            value={this.state.value}
            className="slider"
            id="throttle"
            type="range"
          />
        </div>
        <div className="mining-section__button">
          <button
            type="submit"
            className="btn btn-lg start-button"
            id="minebutton"
            onClick={this.handleClick}
          >{this.state.isToggleOn ? 'Stop Mining' : 'Start Mining'}</button>
          <button
            type="submit"
            className="btn btn-lg stop-button"
            id="minebutton"
            onClick={this.handleStop}
          >Stop Mining</button>
        </div>
      </div>
    );
  }
}

export default Miner;
