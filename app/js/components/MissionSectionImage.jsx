import React, { Component } from 'react';

class MissionSectionImage extends Component {
  render() {
    return (
      <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
        <div className="mission-section__content-img">
          <img
            className="title-img"
            src="./images/png/MissionImage.png"
            alt="Mission Image"
            // height="660"
            />
        </div>
      </div>
    );
  }
}

export default MissionSectionImage;
