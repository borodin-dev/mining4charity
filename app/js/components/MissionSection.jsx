import React, { Component } from 'react';

class MissionSection extends Component {
  render() {
    return (
      <div className="mission-section__title">
        <div className="background-text">
          <p className="background-text__title">Our mission</p>
        </div>
        <div className="section-title">
          <p className="section-title__text">Our mission</p>
        </div>
      </div>
    );
  }
}

export default MissionSection;
