import React, { Component } from 'react';

class OrganizationsSection extends Component {
  render() {
    return (
      <div className="organizations-section__title">
        <div className="background-text">
          <p className="background-text__title">Supported charity organizations</p>
        </div>
        <div className="section-title">
          <p className="section-title__text">Supported charity organizations</p>
        </div>
      </div>
    );
  }
}

export default OrganizationsSection;
