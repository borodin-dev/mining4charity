import React, { Component } from 'react';


import  HeaderSectionContainer  from '../containers/HeaderSectionContainer';
import  MiningSectionContainer  from '../containers/MiningSectionContainer';
import  NumbersSection  from './NumbersSection';
import  HowItWorksContainer  from '../containers/HowItWorksSectionContainer';
import  OrganizationsSectionContainer  from '../containers/OrganizationsSectionContainer';
import  MissionSectionContainer  from '../containers/MissionSectionContainer';
import  HelpUsSectionContainer from '../containers/HelpUsSectionContainer';
import  SupportersSectionContainer from '../containers/SupportersSectionContainer';
import  FooterSectionContainer from '../containers/FooterSectionContainer';

class App extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <HeaderSectionContainer />
          <MiningSectionContainer />
          <NumbersSection />
          <HowItWorksContainer />
          <OrganizationsSectionContainer />
          <MissionSectionContainer />
          <HelpUsSectionContainer />
          <SupportersSectionContainer />
        </div>
        <FooterSectionContainer />
      </div>

    );
  }
}

export default App;
