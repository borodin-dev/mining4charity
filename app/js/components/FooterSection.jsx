import React, { Component } from 'react';
import Scrollchor from 'react-scrollchor';

class FooterSection extends Component {
  render() {
    return (
      <div className="row footer-section__high">
        <div className="col-lg-3 mail-text">
          <p>For any questions, partnership, offers, support or
            to simply to say hello, please send us an email.</p>
        </div>
        <div className="col-lg-6">
          <nav className="navbar footer-navbar">
            {/*<a className="nav-link" href="#">How it works</a>*/}
            <Scrollchor to="#howWorks" className="nav-link">How it works</Scrollchor>
            {/*<a className="nav-link" href="#">Our mission</a>*/}
            <Scrollchor to="#ourMission" className="nav-link">Our mission</Scrollchor>
            {/*<a className="nav-link" href="#">Donate</a>*/}
            <Scrollchor to="#donate" className="nav-link">Donate</Scrollchor>
            {/*<a className="nav-link" href="#">Our Supporters</a>*/}
            <Scrollchor to="#supporters" className="nav-link">Our Supporters</Scrollchor>
            <a className="nav-link" href="#">Contact</a>
          </nav>
        </div>
        <div className="col-lg-3 mining-footer-text">
          <p>Mining4Charity is a non profit charity organisation with the aim
            to bridge the gap between online support and real world charity.</p>
        </div>
      </div>
    );
  }
}

export default FooterSection;
