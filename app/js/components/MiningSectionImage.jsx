import React, { Component } from 'react';

class MiningSectionImage extends Component {
  render() {
    return (
      <div className="col-12 col-sm-12 col-md-12 col-lg-6 mining-section-image">
        <img
          className="img-fluid"
          src="./images/png/2menbanner_illustration.png"
          alt="Charity"
        />
      </div>
    );
  }
}

export default MiningSectionImage;
