import React, { Component } from 'react';

import Miner from './Miner';

class MiningSection extends Component {
  render() {
    return (
      <div className="col-12 col-sm-12 col-md-12 col-lg-6">
        <Miner/>
      </div>
    );
  }
}

export default MiningSection;
