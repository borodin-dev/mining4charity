import React, { Component } from 'react';

class HelpUsSection extends Component {
    render() {
    return (
      <div className="help-us-section" id="donate">
        <div className="help-us-section__title">
          <div className="background-text">
            <p className="background-text__title">Help us grow</p>
          </div>
          <div className="section-title">
            <p className="section-title__text">Help us grow</p>
          </div>
        </div>
        <div className="row help-us-section__subtitle">
          <div className="col-lg-10 offset-lg-1">Mining4Charity is a stricty non profit initiative and is funded only via donations. To support development
          of the Mining4Charity-project, you can make a donation by sending cryptocurrency to any of the following wallet
            addresses:
          </div>
        </div>

      </div>
    );
  }
}

export default HelpUsSection;
